const { removeModuleScopePlugin, addWebpackAlias, override, babelInclude } = require("customize-cra");
const path = require("path");

module.exports = override(
  removeModuleScopePlugin(),        // (1)
  babelInclude([
    path.resolve("src"),
    path.resolve("../common"),  // (2)
  ]),
  addWebpackAlias({
    ['common']: path.resolve(__dirname, '../common'),
  })
);
