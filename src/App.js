import './App.css';
import Home from './pages/Home';
import Login from './pages/Login';
import Loginback from './pages/Loginback';
import Index from './pages/Index';
import Messagedesktop from './pages/Messagedesktop';
import Mp from './pages/Mp';
import Message from './pages/Message';
import Signup from './pages/Signup';
import Signupparrain from './pages/Signupparrain';
import { BrowserRouter as Router,Route,Switch} from 'react-router-dom';
function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path ='/' component={Index}/>
          <Route path='/home' component={Home}/>
          <Route path='/login'  component={Login}/>
          <Route path='/loginback' component={Loginback}/>
          <Route path='/message-desktop' component={Messagedesktop}/>
          <Route path='/mp' component={Mp} />
          <Route path='/message' component={Message} />
          <Route path='/signup' component={Signup} />
          <Route path ='/signupparrain' component={Signupparrain} />
        </Switch>
      </Router>
      
    </div>
  );
}

export default App;
