import React, {Componnent} from 'react';
import {Link} from 'react-router-dom';
class Home extends React.Component{
    render(){
        return(
            <body id="desktop">
                <div className="popup">
                        <div className="overlay"></div>
                        <img src="./assets/img/home/bg4.png"></img>
                    <div className="d-flex">
                            <p className="sttr">
                                Chérie Chéri vous offre 1 crédit par jour :
                            </p>
                        
                        <div className="pic-1">
                            <img src="./assets/img/logo-fremium.svg"></img>
                        </div>
                        <div className="pic-2">
                            <img src="./assets/img/close.svg" className="close"></img>
                        </div>
                    </div>
                </div>
                <nav id="sidebar">
                    <div className="sidebar-header">
                        <div className="avatar">
                            <img className="img" src="./assets/img/home/photo_2.png" alt=""></img>
                            <p className="bulle">1</p>
                        </div>
                    </div>

                    <ul className="list-unstyled components">
                        <li className="active">
                            <Link to="#">
                                <div className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5.333 11.467L5.333 28 26.667 28 26.667 11.467"></path>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M2.667 13.333L16 4 29.333 13.333M20 28v-8c0-1.473-1.193-2.667-2.667-2.667h-2.666C13.193 17.333 12 18.527 12 20v8"></path>
                                        </g>
                                    </svg>
                                </div>
                                <span>Accueil</span>
                            </Link>
                        </li>
                        <li>
                            <Link to ="/login">
                                <div className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <g fill="none" fill-rule="evenodd">
                                            <g>
                                                <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M20.294 13.91c2.382-.086 4.272-2.034 4.288-4.417-.037-2.332-1.956-4.193-4.288-4.157M12.577 13.91c2.381-.086 4.272-2.034 4.287-4.417-.037-2.332-1.956-4.193-4.287-4.157C10.245 5.3 8.326 7.16 8.289 9.493c.015 2.383 1.906 4.332 4.288 4.418h0zM21.342 26.678V24.01c0-2.947-2.389-5.336-5.335-5.336h-6.67c-2.947 0-5.335 2.39-5.335 5.336v2.668M28.012 26.678v-4.002c0-2.21-1.792-4.002-4.002-4.002" transform="translate(.993 .993)"></path>
                                            </g>
                                        </g>
                                    </svg>
                                    <p className="bulle">1</p>  
                                </div>
                                    <span>Rencontres</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/message">
                                <div className="pic">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                            <g fill="none" fill-rule="evenodd">
                                                <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.437 29.077c3.611 0 6.539-2.928 6.539-6.538 0-3.611-2.927-6.539-6.539-6.539C5.825 16 2.9 18.928 2.9 22.539M9.444 29.084c-.984 0-1.916-.217-2.753-.605l-4.024.854.841-4.032c-.39-.84-.61-1.776-.61-2.762"></path>
                                                <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M8.743 16.037C8.265 14.943 8 13.75 8 12.497c0-5.356 4.797-9.653 10.667-9.653 5.869 0 10.666 4.297 10.666 9.653 0 3.166-1.682 5.952-4.265 7.71.001 1.008-.001 2.366-.001 3.793l-4.186-2.061c-.716.137-1.456.212-2.214.212-.943 0-1.858-.112-2.73-.322"></path>      
                                            </g>
                                        </svg>  
                                    <p className="bulle">99</p>   
                                </div>
                                <span>Conversations</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="#">
                                <div className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M28.012 10.671L4.002 10.671M23.343 17.34c3.315 0 6.003 2.688 6.003 6.003s-2.688 6.003-6.003 6.003-6.002-2.688-6.002-6.003 2.687-6.002 6.002-6.002" transform="translate(.993 .993)"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19.572 28.012H8.003c-2.21 0-4.001-1.792-4.001-4.002V8.003c0-2.21 1.791-4.001 4.001-4.001H24.01c2.21 0 4.002 1.791 4.002 4.001v11.57M23.639 20.395L23.639 23.584M21.041 23.584L23.639 23.584" transform="translate(.993 .993)"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.07 15.34c0 .037-.03.066-.068.066-.036 0-.066-.03-.066-.066 0-.037.03-.067.066-.067M9.002 15.273c.037 0 .067.03.067.067M13.74 15.34c0 .037-.03.066-.068.066-.036 0-.066-.03-.066-.066 0-.037.03-.067.066-.067M13.672 15.273c.037 0 .067.03.067.067M9.07 19.341c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.036.03-.066.066-.066M9.002 19.275c.037 0 .067.03.067.066M13.74 19.341c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.036.03-.066.066-.066M13.672 19.275c.037 0 .067.03.067.066M18.406 15.34c0 .037-.03.066-.066.066-.037 0-.067-.03-.067-.066 0-.037.03-.067.067-.067M18.34 15.273c.036 0 .066.03.066.067M9.07 23.343c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.037.03-.067.066-.067M9.002 23.276c.037 0 .067.03.067.067M13.74 23.343c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.037.03-.067.066-.067M13.672 23.276c.037 0 .067.03.067.067" transform="translate(.993 .993)"/>
                                        </g>
                                    </svg>
                                </div>
                                <span>Agenda</span>
                            </Link>
                        </li>

                        <li>
                            <Link to="#">
                                <div className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M4 16.429c0 3.445 2.624 6.238 5.861 6.238 2.802 0 5.223-2.086 5.806-5.002l.833-4.168c.487-2.428 2.501-4.164 4.833-4.164 3.682 0 6.667 3.178 6.667 7.096"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.666 16.167c-.092 0-.167.074-.166.166 0 .092.075.167.167.167.092 0 .166-.075.166-.167 0-.092-.074-.166-.167-.166M21.666 16.167c-.092 0-.167.074-.166.166 0 .092.075.167.167.167.092 0 .166-.075.166-.167 0-.092-.074-.166-.167-.166"/>
                                            <path stroke="#323232" stroke-width="1.5" d="M24.485 7.515c4.687 4.686 4.687 12.284 0 16.97-4.686 4.687-12.284 4.687-16.97 0-4.687-4.686-4.687-12.284 0-16.97 4.686-4.687 12.284-4.687 16.97 0"/>
                                        </g>
                                    </svg>
                                </div>
                                <span>Coaching</span>
                            </Link>
                        </li>
                    </ul>
                </nav>
                <main className="main home">
                    <div className="container-fluid">
                        <div className="row">
                            <header className="header">
                                <div className="blocL">
                                    <img src="./assets/coach/logo-fremium-copy.svg" alt="" className="img"></img>
                                    <p className="infos"><span>85</span>Cherriz</p>
                                </div>
                            </header>
                            <section className="section">
                                <article className="article">
                                    <div className="top">
                                        <h1 class="ttr">Les Événements Chérie Chéri</h1>
                                        <Link to="#" className="link">Tous les Événements Chérie Chéri
                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 width="143.82px" height="246.865px" viewBox="0 0 143.82 246.865" enable-background="new 0 0 143.82 246.865" xmlspace="preserve">
                                                <path d="M97.604,123.893L4.793,216.049c0,0-10.785,14.053,0,24.837s22.876,4.575,28.758,0s94.94-93.633,102.614-101.307
                                                        c17.595-16.563-0.654-35.295-0.654-35.295L34.859,4.938c0,0-15.686-11.111-28.104,0s1.307,28.758,1.307,28.758L97.604,123.893z"
                                                        ></path>
                                            </svg>
                                        </Link>
                                    </div>
                                </article>

                                <article className="article events">
                                    <div className="swiper-container">
                                        <div className="swiper-wrapper">
                                            <div className="swiper-slide">
                                                <div className="banner">
                                                    <img src="./assets/img/home/bg.png" className="img" alt=""></img>
                                                </div>
                                                    <div className="noir"></div>
                                                    <div className="date">
                                                        <p className="para">SAMEDI 25 SEPTEMBRE</p>
                                                    </div>
                                                <div className="blocBottom">
                                                    <div className="listeParticipants">
                                                        <div className="avatars">
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>                                                    
                                                        </div>
                                                            <Link to="#" class="para">Voir les 38 participants</Link>
                                                    </div>
                                                            <h3 className="sttr">Merveilleuse Soirée<br/>chez Apicius</h3>
                                                            <p className="para">A short, but complet takes up first and second lines sur les berges de seine …</p>
                                                            <Link to="#" className="btn btn-primary">À découvrir</Link>
                                                </div>
                                            </div>
                                        
                                            <div className="swiper-slide">
                                                    <div className="banner">
                                                        <img src="./assets/img/home/bg.png" alt="" className="img"></img>
                                                    </div>
                                                    <div className="noir"></div>
                                                    <div className="date">
                                                        <p class="para">SAMEDI 25 SEPTEMBRE</p>
                                                    </div>
                                                <div className="blocBottom">
                                                    <div className="listeParticipants">
                                                        <div className="avatars">
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                            <div className="avatar">
                                                                <img src="./assets/img/home/photo_2.png" alt="" className="img"></img>
                                                            </div>
                                                        </div>
                                                            <Link to="#" className="para">Voir les 38 participants</Link>
                                                    </div>
                                                            <h3 className="sttr">Merveilleuse Soirée <br/> chez Apicius</h3>
                                                            <p className="para">A short, but complet takes up first and second lines sur les berges de seine …</p>
                                                            <Link to="#" className="btn btn-primary">À découvrir</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </section>

                            <section className="section">
                                <article className="article">
                                    <div className="top">
                                        <h1 className="ttr">Ça ne se raconte pas, ça se vit !</h1>
                                        <Link to="#" className="link">Toutes les galeries 
                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    width="143.82px" height="246.865px" viewBox="0 0 143.82 246.865" enable-background="new 0 0 143.82 246.865"
                                                    xmlspace="preserve">
                                                <path d="M97.604,123.893L4.793,216.049c0,0-10.785,14.053,0,24.837s22.876,4.575,28.758,0s94.94-93.633,102.614-101.307
                                                    c17.595-16.563-0.654-35.295-0.654-35.295L34.859,4.938c0,0-15.686-11.111-28.104,0s1.307,28.758,1.307,28.758L97.604,123.893z">
                                                </path>
                                            </svg>
                                        </Link>
                                    </div>
                                </article>
                                <article className="article galerie">
                                    <div className="swiper-container">
                                        <div className="swiper-wrapper">
                                            <div className="swiper-slide">
                                                <div className="d-flex">
                                                    <div className="picSingle">
                                                        <div className="pic">
                                                            <img src="./assets/img/home/bitmap_4.png" alt=""></img>
                                                        </div>                                
                                                    </div>
                                                    <div className="picGroup">
                                                        <div className="pic">
                                                            <img src="./assets/img/home/bitmap_2.png" alt=""></img>
                                                        </div>
                                                        <div className="pic">
                                                            <img src="./assets/img/home/bitmap_3.png" alt=""></img>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div className="swiper-slide">
                                                <div className="d-flex">
                                                    <div className="picGroup">
                                                        <div className="pic">
                                                            <img src="./assets/img/home/bitmap_2.png" alt=""></img>
                                                        </div>
                                                        <div className="pic">
                                                            <img src="./assets/img/home/bitmap_3.png" alt=""></img>
                                                        </div>
                                                    </div>
                                                    <div className="picSingle">
                                                        <div className="pic">
                                                            <img src="./assets/img/home/bitmap_4.png" alt=""></img>
                                                        </div>                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </article>
                            </section>

                            <section className="section"> 
                                <article className="article">
                                    <div className="top">
                                        <h1 class="ttr">Echanger avec les Chéri(e)s
                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="313.104px" height="325px" viewBox="0 0 313.104 325" enable-background="new 0 0 313.104 325" xmlspace="preserve">
                                                <g>
                                                    <path d="M57.052,52.189V12.821c0-7.081-5.42-12.82-12.5-12.82s-12.5,5.739-12.5,12.82V52.19C14.052,57.707,0,74.867,0,95.192
                                                        c0,20.326,14.052,37.486,32.052,43.003V312.18c0,7.081,5.42,12.82,12.5,12.82s12.5-5.739,12.5-12.82V138.195
                                                        c19-5.518,32.051-22.678,32.051-43.003C89.103,74.867,76.052,57.707,57.052,52.189z M57.513,113.6
                                                        c-3.634,2.536-8.052,4.028-12.82,4.028s-9.186-1.492-12.82-4.028c-5.812-4.055-9.615-10.784-9.615-18.408
                                                        c0-7.622,3.805-14.353,9.615-18.406c3.635-2.537,8.052-4.028,12.82-4.028s9.187,1.491,12.82,4.028
                                                        c5.812,4.054,9.616,10.784,9.616,18.406C67.129,102.815,63.324,109.545,57.513,113.6z"></path>
                                                    <path d="M281.052,52.189V12.821c0-7.081-5.42-12.82-12.5-12.82s-12.5,5.739-12.5,12.82V52.19C237.052,57.707,224,74.867,224,95.192
                                                        c0,20.326,13.051,37.486,32.051,43.003V312.18c0,7.081,5.42,12.82,12.5,12.82s12.5-5.739,12.5-12.82V138.195
                                                        c18-5.518,32.052-22.678,32.052-43.003C313.104,74.867,299.052,57.707,281.052,52.189z M281.231,113.6
                                                        c-3.635,2.536-8.052,4.028-12.82,4.028s-9.187-1.492-12.82-4.028c-5.812-4.055-9.616-10.784-9.616-18.408
                                                        c0-7.622,3.805-14.353,9.616-18.406c3.634-2.537,8.052-4.028,12.82-4.028s9.186,1.491,12.82,4.028
                                                        c5.812,4.054,9.615,10.784,9.615,18.406C290.847,102.815,287.042,109.545,281.231,113.6z"></path>
                                                    <path d="M169.052,186.805V12.821c0-7.081-5.418-12.82-12.5-12.82c-7.08,0-12.5,5.739-12.5,12.82v173.984
                                                        c-19,5.518-32.051,22.678-32.051,43.003c0,20.326,13.051,37.485,32.051,43.003v39.369c0,7.081,5.42,12.819,12.5,12.819
                                                        c7.082,0,12.5-5.738,12.5-12.819v-39.369c19-5.518,32.052-22.677,32.052-43.003S188.052,192.322,169.052,186.805z M169.052,248.215
                                                        c-3.634,2.537-8.052,4.028-12.82,4.028s-9.186-1.491-12.82-4.028c-5.812-4.055-9.615-10.785-9.615-18.407
                                                        c0-7.623,3.805-14.353,9.615-18.408c3.635-2.536,8.052-4.026,12.82-4.026s9.187,1.49,12.82,4.026
                                                        c5.811,4.057,9.615,10.785,9.615,18.408C178.667,237.43,174.862,244.16,169.052,248.215z"></path>
                                                </g>
                                            </svg>
                                        </h1>
                                        <Link to="#" class="link">Tous et toutes les Chéri(e)s  
                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlnsxlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    width="143.82px" height="246.865px" viewBox="0 0 143.82 246.865" enable-background="new 0 0 143.82 246.865"
                                                    xmlspace="preserve">
                                                <path d="M97.604,123.893L4.793,216.049c0,0-10.785,14.053,0,24.837s22.876,4.575,28.758,0s94.94-93.633,102.614-101.307
                                                c17.595-16.563-0.654-35.295-0.654-35.295L34.859,4.938c0,0-15.686-11.111-28.104,0s1.307,28.758,1.307,28.758L97.604,123.893z"></path>
                                            </svg>
                                        </Link>
                                    </div>
                                </article>

                                <article className="article echange">
                                    <div className="swiper-container">
                                        <div className="swiper-wrapper">
                                            <div className="swiper-slide d-flex">
                                                <Link to="profil.html" className="picSingle">
                                                    <div className="pic connecter">
                                                        <img src="./assets/img/home/photo.png" alt=""></img>
                                                    </div>
                                                    <div className="name">
                                                        <p className="para">Marcel</p>
                                                    </div>
                                                </Link>  

                                                <Link to="profil.html" className="picSingle">
                                                    <div className="pic premium">
                                                        <img src="./assets/img/home/bitmap2.png" alt=""></img>
                                                    </div>
                                                    <div className="name">
                                                        <p className="para">Julie</p>
                                                    </div>
                                                </Link>  
                                            </div> 

                                            <div className="swiper-slide d-flex">
                                                <Link to="profil.html" className="picSingle">
                                                    <div className="pic">
                                                        <img src="./assets/img/home/photo.png" alt=""></img>
                                                    </div>
                                                    <div className="name">
                                                        <p className="para">Marcel</p>
                                                    </div>
                                                </Link>  
                                                <Link to="profil.html" className="picSingle">
                                                    <div className="pic">
                                                        <img src="./assets/img/home/bitmap_4.png" alt=""></img>
                                                    </div>
                                                    <div className="name">
                                                        <p className="para">Julie</p>
                                                    </div>
                                                </Link>  
                                            </div> 
                                        </div> 
                                    </div> 
                                </article>
                            </section>

                            
                            <section className="section">
                                <article className="article interet">
                                    <div className="pic">
                                        <img src="./assets/img/home/cadeau.svg" alt="" className="img"></img>
                                    </div>
                                    <div className="resume">                            
                                        <h3 className="ttr">Montrer votre intérêt</h3>
                                        <p  className="para">Offrez lui un cadeau qui a du sens</p>
                                    </div>
                                </article> 
                            </section>

                            <section className="section">
                                <article className="article profilAmeliore">
                                    <div className="pic">
                                        <div className="circle">
                                            <img src="./assets/img/home/circle.svg" alt="" className="img"></img>
                                        </div>
                                        <img src="./assets/img/home/oval.png" alt="" className="img"></img>
                                        <div className="progressContent">
                                            <p className="para">75%</p>
                                        </div>
                                    </div>
                                    <div className="resume">
                                        <h3 className="ttr">Améliorez votre profil </h3>
                                        <p  className="para">Avoir l’opportunité de rencontrer d’autres Chéri(e)s et de gagner des Cherriz</p>
                                    </div>
                                </article> 
                            </section>
                            <section className="section">
                                <article className="article coachs">
                                        <div className="pic">
                                            <img src="./assets/img/home/bg2.png" alt="" className="img"></img>
                                        </div>
                                    <div className="resume">
                                        <h3 className="ttr">Cherrissez vous ! <br/>Marie et ses professionnels et coachs prennent soin de vous</h3>
                                        <p className="para">A short, but complete sentence that takes up first and second lines</p>
                                        <div className="picContent">
                                            <div className="pic">
                                                <img src="./assets/img/home/rectangle_3.png" alt="" className="img"></img>
                                            </div>
                                            <div className="pic">
                                                <img src="./assets/img/home/rectangle_2.png" alt="" className="img"></img>
                                            </div>
                                            <div className="pic">
                                                <img src="./ssets/img/home/rectangle.png" alt="" class="img"></img>
                                            </div>
                                            <Link to="#" className="btn decouvrir">
                                                <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    width="143.82px" height="246.865px" viewBox="0 0 143.82 246.865" enable-background="new 0 0 143.82 246.865"
                                                    xmlspace="preserve">
                                                    <path d="M97.604,123.893L4.793,216.049c0,0-10.785,14.053,0,24.837s22.876,4.575,28.758,0s94.94-93.633,102.614-101.307
                                                    c17.595-16.563-0.654-35.295-0.654-35.295L34.859,4.938c0,0-15.686-11.111-28.104,0s1.307,28.758,1.307,28.758L97.604,123.893z"></path>
                                                </svg>
                                                Découvrir <br/>
                                                les coachs
                                            </Link>
                                        </div>
                                        <Link to="#" class="btn btn-primary"> 
                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlnsxlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="296px" height="205px" viewBox="0 0 296 205" enable-background="new 0 0 296 205" xmlspace="preserve">
                                                <g>
                                                    <path d="M268.943,19.16c-12.977,0-18.758,6.073-18.758,6.073L210,59.607V45.881C210,18.936,190.772,0,163.827,0H51.967
                                                        C25.021,0,0,18.936,0,45.881v107.031C0,179.857,25.021,205,51.967,205h111.86C190.772,205,210,179.857,210,152.912v-13.387
                                                        l40.841,34.374c0,0,4.473,6.073,17.45,6.073S296,170.052,296,151.341c0-7.063,0-96.487,0-103.55
                                                        C296,29.081,281.922,19.16,268.943,19.16z M188,152.537c0,21.288-8.596,31.463-29.882,31.463H55.262
                                                        C33.976,184,22,173.825,22,152.537V46.859C22,25.572,33.976,23,55.262,23h102.855C179.404,23,188,25.572,188,46.859V152.537z
                                                        M275.839,154.17c-0.253,1.085-2.921,6.287-7.899,3.471C262.508,153.265,211,111.468,211,111.468V87.627l57.09-46.475
                                                        c0,0,6.666-1.357,7.118,3.471C275.661,49.451,276.092,153.085,275.839,154.17z"></path>
                                                </g>
                                            </svg>
                                            Voir les videos de Marie
                                        </Link>
                                    </div>
                                </article> 
                            </section> 

                            <section className="section">
                                <article className="article cheriecheri">
                                    <div className="pic">
                                        <img src="./assets/img/home/illustration.svg" alt="" className="img"></img>
                                    </div>
                                   <div className="resume">
                                        <h3 className="ttr">Chérie Chéri s’engage pour l’écologie  !</h3>
                                        <p className="para">A short, but complete sentence<br/> that takes up  second lines</p>
                                        <Link to="#" className="link">En savoir plus  
                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlnsxlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                    width="143.82px" height="246.865px" viewBox="0 0 143.82 246.865" enable-background="new 0 0 143.82 246.865"
                                                    xmlspace="preserve">
                                                <path d="M97.604,123.893L4.793,216.049c0,0-10.785,14.053,0,24.837s22.876,4.575,28.758,0s94.94-93.633,102.614-101.307
                                                c17.595-16.563-0.654-35.295-0.654-35.295L34.859,4.938c0,0-15.686-11.111-28.104,0s1.307,28.758,1.307,28.758L97.604,123.893z"></path>
                                            </svg>
                                        </Link>
                                    </div>
                                </article>
                            </section>
                            
                            <section className="section">
                                <article className="article art">
                                    <h3 className="ttr">Spécialement pour vous !</h3>
                                    <div className="resume">
                                        <div className="pic">
                                            <img src="./assets/img/home/guerlain.png" alt="" className="img"></img>
                                        </div>
                                        <p className="para">L'ART ET <br/>
                                            LA MATIÈRE</p>
                                        <div className="pic">
                                            <img src="./assets/img/home/arma-franchise-visual.jpg" alt="" className="img"></img>
                                        </div>
                                    </div>
                                </article>
                            </section>
                        </div>
                    </div>
                </main>
            </body>
        )
    }
}
export default Home