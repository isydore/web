import React, {Component} from 'react';
import { Link } from 'react-router-dom';
class Index extends React.Component{
render() {
    return (
        <div>
           <main className="container-fluid" id="splashscreen">
            <div className="bg">
                <img src="./assets/img/bg.png" alt=""></img>
            </div>
            <fieldset id="splash">
                <section className="title">
                    <h1 className="ttr">Rencontrez et Vibrez avec du sens</h1>
                </section>
                <section className="logo">
                    <img className="" src="./assets/img/logo.png" alt=""></img>
                </section>
            </fieldset>
            <fieldset id="login">
            <header>
                        <Link to="/Loginback" className="btnlogin">
                            Connexion
                        </Link>
                    </header>
                {/* <header><a className="btnlogin" href="loginback.html">connexion</a></header> */}
            <section className="title">
                <h2 className="sttr">Bienvenue <br/> parmi les <br/>Cheri(e)s</h2>
            </section>
            <section className="login">
                
                <Link to="/home" className="btn facebook d-flex">
                    <i className="fab fa-facebook-f"></i>
                    <span>Continuer avec Facebook</span>
                </Link>
                <Link to="/home" className="btn linkedin d-flex">
                    <i className="fab fa-linkedin-in"></i>
                    <span>Continuer avec Linkedin</span>
                </Link>
              
                <Link to="/home" className="btn apple d-flex">
                    <i className="fab fa-apple"></i>
                    <span>Continuer avec Apple</span>
                </Link>
             
            </section>
            <footer>
                <p className="para">
                    Nous ne publions rien sur ta page facebook ou linkedin.<br/> 
                    En continuant tu acceptes nos conditions de service et notre politique de confidentialité
                </p>
            </footer>
            </fieldset>
        </main>
        </div>
        )
    }
}
export default Index;