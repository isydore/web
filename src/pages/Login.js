import React,{Component} from 'react';
import { Link } from 'react-router-dom';
class Login extends React.Component{
    render(){
        return(
            <div>
                <main className="container-fluid" id="login">
                    <header>
                        <Link to="/loginback" className="btnlogin">
                            Connexion
                        </Link>
                    </header>
                    <div className="bg">
                        <img src="./assets/img/bg.png" alt=""></img>
                    </div>
                    <section className="title">
                        <h2 className="sttr">Bienvenue <br/> parmi les <br/>Cheri(e)s</h2>
                    </section>
                    <section className="login">
                        <Link to="/signup" className="btn facebook d-flex">
                            <i className="fab fa-facebook-f"></i>
                            <span>Continuer avec Facebook</span>
                        </Link>
                        <Link to="/signup" className="btn linkedin d-flex">
                            <i className="fab fa-linkedin-in"></i>
                            <span>Continuer avec Linkedin</span>
                        </Link>
                    
                        <Link to="/signup" className="btn apple d-flex">
                            <i className="fab fa-apple"></i>
                            <span>Continuer avec Apple</span>
                        </Link>
                    </section>
                    <footer>
                        <p className="para">
                            Nous ne publions rien sur ta page facebook ou linkedin.<br/> 
                            En continuant tu acceptes nos conditions de services et notre politiques de confidentialité
                        </p>
                    </footer>
                </main>
            </div>
        )
    }
}
export default Login;