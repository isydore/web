import React, {Component} from 'react';
import { Link } from 'react-router-dom';

class Loginback extends React.Component{
    render(){
        return(
            <div>
                <main className="container-fluid" id="loginBack">
                    <div className="bg">
                        <img src="./assets/img/bg.png" alt=""></img>
                    </div>
                    <header>
                        <Link to="/login" className="btnlogin">
                            retour
                        </Link>
                        {/* <a className="btnlogin" href="index.html">retour</a> */}
                    </header>
                    <section className="title">
                        <h2 className="sttr">A nouveau parmi nous?</h2>
                    </section>
                    <section className="login">
                        <Link to="/signup" className="btn facebook d-flex">
                            <i className="fab fa-facebook-f"></i>
                            <span>Continuer avec Facebook</span>
                        </Link>
                        <Link to="/signup" className="btn linkedin d-flex">
                            <i className="fab fa-linkedin-in"></i>
                            <span>Continuer avec Linkedin</span>
                        </Link>
                    
                        <Link to="/signup" className="btn apple d-flex">
                            <i className="fab fa-apple"></i>
                            <span>Continuer avec Apple</span>
                        </Link>
                    </section>
                    <div className="row">
                        <span className="separator"></span>
                        <span className="ou">ou</span>
                        <span className="separator"></span>
                    </div>
                    <section className="formLogin">
                        <form action="signup.html">
                            <div className="form-group">
                                <input type="email" className="form-control" id="email" placeholder="Email"></input>
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" id="pwd" placeholder="Mot de passe"></input>
                            </div>
                            <button type="submit" className="btn btn-default">Se connecter avec un mail</button>
                            <div className="forgot">
                                <Link to="" >
                                    Mot de passe oublié
                                </Link>
                                {/* <a className="" href="">Mot de passe oublié</a> */}
                            </div>
                        </form>
                    </section>        
                </main>
            </div>
        )
    }
}
export default Loginback;