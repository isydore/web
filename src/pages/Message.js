import React, {Component} from 'react';
import {Link} from 'react-router-dom';
class Message extends Component{
    render(){
        return(
            <div>
                <main className="main message"> 
                    <div className="container-fluid">
                        <div className="row">
                            <header className="header">
                                <div className="form">
                                    <img src="./assets/img/home/icones-loupe.svg" alt="" className="loupe"/>
                                    <input type="text" className="search" placeholder="Recherche"/>
                                    <img src="./assets/img/home/icones-filtres.svg" alt="" className="filtre"/>
                                </div>
                            </header>
                            <section className="section">
                                <article>
                                    <h2 className="ttr">Vos messages</h2>
                                </article>
                                <article className="chat">
                                    <div className="fond">
                                        <div className="d-flex">
                                            <div className="pic">
                                                <Link to="mp.html">
                                                    <img src="./assets/img/home/bitmap.png" alt=""></img>
                                                </Link>
                                            </div>
                                            <div className="text">
                                                <h4 className="sttr connecter">Marcel</h4>
                                                <p className="para">So Excited for this!</p>
                                            </div>
                                            <div className="heure">
                                                <p className="para">11h45</p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="action">
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <g stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                                            <path d="M18 8.91h0c0 4.921-4.029 8.91-9 8.91h0c-4.971 0-9-3.989-9-8.91h0C0 3.989 4.029 0 9 0h0c4.971 0 9 3.989 9 8.91z" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                            <path d="M8.625 8.911c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 8.538 9 8.538s-.375.167-.375.373M8.625 4.951c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 4.578 9 4.578s-.375.167-.375.373M8.625 12.871c0 .205.168.371.375.37.207 0 .375-.167.375-.372s-.168-.371-.375-.371-.375.167-.375.373" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg> 
                                            </Link>                              
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                <g fill="none" fill-rule="evenodd">
                                                    <g>
                                                        <g>
                                                            <g filter="" transform="translate(-306 -375) translate(179 338) translate(107)">
                                                                <g>
                                                                    <g>
                                                                        <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5 20.79L5 3.891M19 13.88L19 3.96M5 13.86s.875-.72 3.5-.72 4.375 1.71 7 1.71 3.5-.967 3.5-.967M5 3.893s.875-.923 3.5-.923 4.375 1.71 7 1.71 3.5-.72 3.5-.72" transform="translate(20 37.62)"/>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </Link>
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-373 -375) translate(179 338) translate(171)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M18 5.94v12.623c0 1.23-1.027 2.227-2.269 2.227h-7.5C6.988 20.79 6 19.793 6 18.563V5.94M19.5 5.94L4.5 5.94M10 2.97L14 2.97M14 9.9L14 16.83M10 16.83L10 9.9" transform="translate(23 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>  
                                            </Link>                              
                                        </li>
                                    </ul>
                                </article>
                                <article className="chat">
                                    <div className="fond">
                                        <div className="d-flex">
                                            <div className="pic">
                                                <Link to="/mp">
                                                    <img src="./assets/img/home/bitmap.png" alt=""></img>
                                                </Link>
                                            </div>
                                            <div className="text">
                                                <h4 className="sttr connecter">Marcel</h4>
                                                <p className="para connecter">So Excited for this!</p>
                                            </div>
                                            <div className="heure connecter">
                                                <p className="para">11h45</p>
                                            </div>
                                        </div>
                                    </div>
                                        <ul className="action">
                                            <li>
                                                <Link to="#" className="click">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                                        <g fill="none" fill-rule="evenodd">
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <g>
                                                                            <g stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                                                <path d="M18 8.91h0c0 4.921-4.029 8.91-9 8.91h0c-4.971 0-9-3.989-9-8.91h0C0 3.989 4.029 0 9 0h0c4.971 0 9 3.989 9 8.91z" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                                <path d="M8.625 8.911c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 8.538 9 8.538s-.375.167-.375.373M8.625 4.951c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 4.578 9 4.578s-.375.167-.375.373M8.625 12.871c0 .205.168.371.375.37.207 0 .375-.167.375-.372s-.168-.371-.375-.371-.375.167-.375.373" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg> 
                                                </Link>                              
                                            </li>
                                            <li>
                                                <Link to="#" className="click">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                        <g fill="none" fill-rule="evenodd">
                                                            <g>
                                                                <g>
                                                                    <g filter="" transform="translate(-306 -375) translate(179 338) translate(107)">
                                                                        <g>
                                                                            <g>
                                                                                <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5 20.79L5 3.891M19 13.88L19 3.96M5 13.86s.875-.72 3.5-.72 4.375 1.71 7 1.71 3.5-.967 3.5-.967M5 3.893s.875-.923 3.5-.923 4.375 1.71 7 1.71 3.5-.72 3.5-.72" transform="translate(20 37.62)"/>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="#" className="click">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                        <g fill="none" fill-rule="evenodd">
                                                            <g>
                                                                <g>
                                                                    <g filter="" transform="translate(-373 -375) translate(179 338) translate(171)">
                                                                        <g>
                                                                            <g>
                                                                                <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M18 5.94v12.623c0 1.23-1.027 2.227-2.269 2.227h-7.5C6.988 20.79 6 19.793 6 18.563V5.94M19.5 5.94L4.5 5.94M10 2.97L14 2.97M14 9.9L14 16.83M10 16.83L10 9.9" transform="translate(23 37.62)"/>
                                                                            </g>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>  
                                                </Link>                              
                                            </li>
                                        </ul>
                                </article>
                                <article className="chat">
                                    <div className="fond">
                                        <div className="d-flex">
                                            <div className="pic">
                                                <Link to="/mp">
                                                    <img src="./assets/img/home/bitmap.png" alt=""></img>
                                                </Link>
                                            </div>
                                            <div className="text">
                                                <h4 className="sttr connecter">Marcel</h4>
                                                <p className="para">So Excited for this!</p>
                                            </div>
                                            <div className="heure">
                                                <p className="para">11h45</p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="action">
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <g stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                                            <path d="M18 8.91h0c0 4.921-4.029 8.91-9 8.91h0c-4.971 0-9-3.989-9-8.91h0C0 3.989 4.029 0 9 0h0c4.971 0 9 3.989 9 8.91z" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                            <path d="M8.625 8.911c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 8.538 9 8.538s-.375.167-.375.373M8.625 4.951c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 4.578 9 4.578s-.375.167-.375.373M8.625 12.871c0 .205.168.371.375.37.207 0 .375-.167.375-.372s-.168-.371-.375-.371-.375.167-.375.373" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg> 
                                            </Link>                              
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-306 -375) translate(179 338) translate(107)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5 20.79L5 3.891M19 13.88L19 3.96M5 13.86s.875-.72 3.5-.72 4.375 1.71 7 1.71 3.5-.967 3.5-.967M5 3.893s.875-.923 3.5-.923 4.375 1.71 7 1.71 3.5-.72 3.5-.72" transform="translate(20 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-373 -375) translate(179 338) translate(171)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M18 5.94v12.623c0 1.23-1.027 2.227-2.269 2.227h-7.5C6.988 20.79 6 19.793 6 18.563V5.94M19.5 5.94L4.5 5.94M10 2.97L14 2.97M14 9.9L14 16.83M10 16.83L10 9.9" transform="translate(23 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>  
                                            </Link>                              
                                        </li>
                                    </ul>
                                </article>
                                <article className="chat">
                                    <div className="fond">
                                        <div className="d-flex">
                                            <div className="pic">
                                                <Link to="mp.html">
                                                    <img src="./assets/img/home/bitmap.png" alt=""></img>
                                                </Link>
                                            </div>
                                            <div className="text">
                                                <h4 className="sttr connecter">Marcel</h4>
                                                <p className="para">So Excited for this!</p>
                                            </div>
                                            <div className="heure">
                                                <p className="para">11h45</p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="action">
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <g stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                                            <path d="M18 8.91h0c0 4.921-4.029 8.91-9 8.91h0c-4.971 0-9-3.989-9-8.91h0C0 3.989 4.029 0 9 0h0c4.971 0 9 3.989 9 8.91z" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                            <path d="M8.625 8.911c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 8.538 9 8.538s-.375.167-.375.373M8.625 4.951c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 4.578 9 4.578s-.375.167-.375.373M8.625 12.871c0 .205.168.371.375.37.207 0 .375-.167.375-.372s-.168-.371-.375-.371-.375.167-.375.373" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg> 
                                            </Link>                              
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-306 -375) translate(179 338) translate(107)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5 20.79L5 3.891M19 13.88L19 3.96M5 13.86s.875-.72 3.5-.72 4.375 1.71 7 1.71 3.5-.967 3.5-.967M5 3.893s.875-.923 3.5-.923 4.375 1.71 7 1.71 3.5-.72 3.5-.72" transform="translate(20 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-373 -375) translate(179 338) translate(171)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M18 5.94v12.623c0 1.23-1.027 2.227-2.269 2.227h-7.5C6.988 20.79 6 19.793 6 18.563V5.94M19.5 5.94L4.5 5.94M10 2.97L14 2.97M14 9.9L14 16.83M10 16.83L10 9.9" transform="translate(23 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>  
                                            </Link>                              
                                        </li>
                                    </ul>
                                </article>
                                <article className="chat">
                                    <div className="fond">
                                        <div className="d-flex">
                                            <div className="pic">
                                                <Link to="/mp">
                                                    <img src="./assets/img/home/bitmap.png" alt=""></img>
                                                </Link>
                                            </div>
                                            <div className="text">
                                                <h4 className="sttr connecter">Marcel</h4>
                                                <p className="para">So Excited for this!</p>
                                            </div>
                                            <div className="heure">
                                                <p className="para">11h45</p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="action">
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <g stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                                            <path d="M18 8.91h0c0 4.921-4.029 8.91-9 8.91h0c-4.971 0-9-3.989-9-8.91h0C0 3.989 4.029 0 9 0h0c4.971 0 9 3.989 9 8.91z" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                            <path d="M8.625 8.911c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 8.538 9 8.538s-.375.167-.375.373M8.625 4.951c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 4.578 9 4.578s-.375.167-.375.373M8.625 12.871c0 .205.168.371.375.37.207 0 .375-.167.375-.372s-.168-.371-.375-.371-.375.167-.375.373" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg> 
                                            </Link>                              
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-306 -375) translate(179 338) translate(107)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5 20.79L5 3.891M19 13.88L19 3.96M5 13.86s.875-.72 3.5-.72 4.375 1.71 7 1.71 3.5-.967 3.5-.967M5 3.893s.875-.923 3.5-.923 4.375 1.71 7 1.71 3.5-.72 3.5-.72" transform="translate(20 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-373 -375) translate(179 338) translate(171)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M18 5.94v12.623c0 1.23-1.027 2.227-2.269 2.227h-7.5C6.988 20.79 6 19.793 6 18.563V5.94M19.5 5.94L4.5 5.94M10 2.97L14 2.97M14 9.9L14 16.83M10 16.83L10 9.9" transform="translate(23 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>  
                                            </Link>                              
                                        </li>
                                    </ul>
                                </article>
                                <article className="chat">
                                    <div className="fond">
                                        <div className="d-flex">
                                            <div className="pic">
                                                <Link to="mp.html">
                                                    <img src="img/home/bitmap.png" alt=""></img>
                                                </Link>
                                            </div>
                                            <div className="text">
                                                <h4 className="sttr connecter">Marcel</h4>
                                                <p className="para">So Excited for this!</p>
                                            </div>
                                            <div className="heure">
                                                <p className="para">11h45</p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul className="action">
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g>
                                                                    <g>
                                                                        <g stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                                            <path d="M18 8.91h0c0 4.921-4.029 8.91-9 8.91h0c-4.971 0-9-3.989-9-8.91h0C0 3.989 4.029 0 9 0h0c4.971 0 9 3.989 9 8.91z" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                            <path d="M8.625 8.911c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 8.538 9 8.538s-.375.167-.375.373M8.625 4.951c0 .205.168.371.375.37.207 0 .375-.167.375-.372S9.207 4.578 9 4.578s-.375.167-.375.373M8.625 12.871c0 .205.168.371.375.37.207 0 .375-.167.375-.372s-.168-.371-.375-.371-.375.167-.375.373" transform="translate(-239 -375) translate(179 338) rotate(-90 61 1) translate(3 3.97)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg> 
                                            </Link>                              
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-306 -375) translate(179 338) translate(107)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5 20.79L5 3.891M19 13.88L19 3.96M5 13.86s.875-.72 3.5-.72 4.375 1.71 7 1.71 3.5-.967 3.5-.967M5 3.893s.875-.923 3.5-.923 4.375 1.71 7 1.71 3.5-.72 3.5-.72" transform="translate(20 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link to="#" className="click">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25">
                                                    <g fill="none" fill-rule="evenodd">
                                                        <g>
                                                            <g>
                                                                <g filter="" transform="translate(-373 -375) translate(179 338) translate(171)">
                                                                    <g>
                                                                        <g>
                                                                            <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M18 5.94v12.623c0 1.23-1.027 2.227-2.269 2.227h-7.5C6.988 20.79 6 19.793 6 18.563V5.94M19.5 5.94L4.5 5.94M10 2.97L14 2.97M14 9.9L14 16.83M10 16.83L10 9.9" transform="translate(23 37.62)"/>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>  
                                            </Link>                              
                                        </li>
                                    </ul>
                                </article>
                            </section>
                            <footer className="footer">
                                <Link to="/home" className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">

                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M5.333 11.467L5.333 28 26.667 28 26.667 11.467"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M2.667 13.333L16 4 29.333 13.333M20 28v-8c0-1.473-1.193-2.667-2.667-2.667h-2.666C13.193 17.333 12 18.527 12 20v8"/>
                                        </g>
                                    </svg>
                                </Link>
                                <Link to="#" className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">

                                        <g fill="none" fill-rule="evenodd">
                                            <g>
                                                <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M20.294 13.91c2.382-.086 4.272-2.034 4.288-4.417-.037-2.332-1.956-4.193-4.288-4.157M12.577 13.91c2.381-.086 4.272-2.034 4.287-4.417-.037-2.332-1.956-4.193-4.287-4.157C10.245 5.3 8.326 7.16 8.289 9.493c.015 2.383 1.906 4.332 4.288 4.418h0zM21.342 26.678V24.01c0-2.947-2.389-5.336-5.335-5.336h-6.67c-2.947 0-5.335 2.39-5.335 5.336v2.668M28.012 26.678v-4.002c0-2.21-1.792-4.002-4.002-4.002" transform="translate(.993 .993)"/>
                                            </g>
                                        </g>
                                    </svg>
                                    <p className="bulle">1</p>
                                </Link>
                                <Link to="/message" className="pic active">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.437 29.077c3.611 0 6.539-2.928 6.539-6.538 0-3.611-2.927-6.539-6.539-6.539C5.825 16 2.9 18.928 2.9 22.539M9.444 29.084c-.984 0-1.916-.217-2.753-.605l-4.024.854.841-4.032c-.39-.84-.61-1.776-.61-2.762"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M8.743 16.037C8.265 14.943 8 13.75 8 12.497c0-5.356 4.797-9.653 10.667-9.653 5.869 0 10.666 4.297 10.666 9.653 0 3.166-1.682 5.952-4.265 7.71.001 1.008-.001 2.366-.001 3.793l-4.186-2.061c-.716.137-1.456.212-2.214.212-.943 0-1.858-.112-2.73-.322"/>        
                                        </g>
                                    </svg>
                                    <p className="bulle">99</p>
                                </Link>
                                <Link to="#" className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M28.012 10.671L4.002 10.671M23.343 17.34c3.315 0 6.003 2.688 6.003 6.003s-2.688 6.003-6.003 6.003-6.002-2.688-6.002-6.003 2.687-6.002 6.002-6.002" transform="translate(.993 .993)"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M19.572 28.012H8.003c-2.21 0-4.001-1.792-4.001-4.002V8.003c0-2.21 1.791-4.001 4.001-4.001H24.01c2.21 0 4.002 1.791 4.002 4.001v11.57M23.639 20.395L23.639 23.584M21.041 23.584L23.639 23.584" transform="translate(.993 .993)"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.07 15.34c0 .037-.03.066-.068.066-.036 0-.066-.03-.066-.066 0-.037.03-.067.066-.067M9.002 15.273c.037 0 .067.03.067.067M13.74 15.34c0 .037-.03.066-.068.066-.036 0-.066-.03-.066-.066 0-.037.03-.067.066-.067M13.672 15.273c.037 0 .067.03.067.067M9.07 19.341c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.036.03-.066.066-.066M9.002 19.275c.037 0 .067.03.067.066M13.74 19.341c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.036.03-.066.066-.066M13.672 19.275c.037 0 .067.03.067.066M18.406 15.34c0 .037-.03.066-.066.066-.037 0-.067-.03-.067-.066 0-.037.03-.067.067-.067M18.34 15.273c.036 0 .066.03.066.067M9.07 23.343c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.037.03-.067.066-.067M9.002 23.276c.037 0 .067.03.067.067M13.74 23.343c0 .037-.03.067-.068.067-.036 0-.066-.03-.066-.067 0-.037.03-.067.066-.067M13.672 23.276c.037 0 .067.03.067.067" transform="translate(.993 .993)"/>
                                        </g>
                                    </svg>
                                </Link>
                                <Link to="#" className="pic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                                        <g fill="none" fill-rule="evenodd">
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M4 16.429c0 3.445 2.624 6.238 5.861 6.238 2.802 0 5.223-2.086 5.806-5.002l.833-4.168c.487-2.428 2.501-4.164 4.833-4.164 3.682 0 6.667 3.178 6.667 7.096"/>
                                            <path stroke="#323232" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.666 16.167c-.092 0-.167.074-.166.166 0 .092.075.167.167.167.092 0 .166-.075.166-.167 0-.092-.074-.166-.167-.166M21.666 16.167c-.092 0-.167.074-.166.166 0 .092.075.167.167.167.092 0 .166-.075.166-.167 0-.092-.074-.166-.167-.166"/>
                                            <path stroke="#323232" stroke-width="1.5" d="M24.485 7.515c4.687 4.686 4.687 12.284 0 16.97-4.686 4.687-12.284 4.687-16.97 0-4.687-4.686-4.687-12.284 0-16.97 4.686-4.687 12.284-4.687 16.97 0"/>
                                        </g>
                                    </svg>
                                </Link>
                            </footer>
                        </div>
                    </div>
                </main>
            </div>
        )
    }
}
export default Message;