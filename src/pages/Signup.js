import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Signup extends Component{
    render(){
        return(
            <div>
                 <main className="container-fluid" id="signupok2">
                    <div className="bg">
                        <img src="./assets/img/photo.png" alt=""></img>
                    </div>
                    <section className="texte">
                        <article>
                            <img className="logo" src="img/logo.svg" alt=""></img>
                            <h1 className="ttr">Bienvenue chez les Chéri(e)s </h1>
                            <div className="info d-flex">
                                <div className="pic">
                                    <img className="" src="./assests/img/romance-wedding-champaine-glasses-valentine-s-day-wedding-wine.svg" alt=""></img>
                                </div>
                                <p className="sttr">Une occasion de participer 
                                    à la nouvelle expérience digitale de la communauté. </p>
                            </div>
                            <div className="info d-flex">
                                <div className="pic">
                                    <img className="" src="./assets/img/romance-wedding-gift-box.svg" alt=""></img>
                                </div>
                                <p className="sttr">En complétant votre profil  vous deviendrez un utilisateur VIP et gagnerez des crédits qui vous 
                                    serviront  dans la future application.</p>
                            </div>
                        </article>
                        <article>
                            <div className="pad">
                                <Link to="/signupparrain" className="btn">Je complète mon profil</Link>
                            </div>
                        </article>
                    </section>
                 </main>
            </div>
        )
    }
}
export default Signup;