import React, {Component} from 'react'
import {Link} from 'react-router-dom';

class Signupparain extends Component{
    render(){
        return(
            <div>
                <main className="container-fluid" id="signup">
                    <form action="home.html" id="signupForm">
                        <fieldset className="stepone">
                            <section className="title">
                                <h2 className="ttr">Mon parrain</h2>
                            </section>
                            <section className="FormSignUp">
                                <div className="form-group">
                                    <label className="sttr">Mon Code Parrainage</label>
                                    <input type="email" className="form-control" id="code" placeholder="Email"></input>
                                </div>
                                <div className="erreur"><p className="">Ce champ de saisie est dédié au code promo donné par un membre de la communauté Chérie Chéri.</p></div>
                            </section>
                            <section className="control">
                                <Link to="#" className="float-left prev"></Link>
                                <Link to="#" className="float-right next"></Link>
                            </section>
                        </fieldset>
                        <fieldset className="steptwo">
                            <section className="title">
                                <h2 className="ttr">Mon identité</h2>
                            </section>
                            <section className="FormSignUp">
                                <div className="form-group">
                                    <input type="text" className="form-control" id="nom" placeholder="Mon nom"></input>
                                </div>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="email" placeholder="Mon Lieu de Résidence"></input>
                                </div>
                                <div className="form-group">
                                    <label className="sttr date">Ma Date d'anniversaire*</label>
                                    <input type="date" className="form-control" id="date" placeholder="Email"></input>
                                </div>
                            </section>
                            <section className="control">
                                <Link to="#" className="float-left prev"></Link>
                                <Link to="#" className="float-right next"></Link>
                            </section>
                        </fieldset>
                        <fieldset className="stepthree">
                            <section className="title">
                                <h2 className="ttr">Je recherche</h2>
                            </section>
                                <section className="FormSignUp">
                                    <div className="form-group">
                                        <label className="sttr">Une photo de profil c'est mieux pour être un ou une parfait(e) Chéri(e)</label>
                                        <div className="input-group">
                                            <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">De vraies relations amoureuses</button>
                                            <input type="checkbox" className="hidden" />
                                            </span>
                                            <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Pas encore prêt(e) à m'engager</button>
                                            <input type="checkbox" className="hidden" />
                                            </span>
                                            <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Laissons la vie faire les choses</button>
                                            </span>
                                            <span className="button-checkbox input-group-btn">
                                                <input type="checkbox" className="hidden"/>
                                                <button type="button" className="btn" data-color="primary">Pas intéréssé(e)</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="sttr half">Elargir mon cercle professionel</label>
                                        <label className="yes-no-checkbox float-right">
                                            <input className="yes-no-checkbox__input" checked="" type="checkbox" name="" id="" ></input>
                                            <div className="yes-no-checkbox__checkbox"></div>
                                        </label>
                                    </div>
                                    <div className="form-group">
                                        <label className="sttr half">Se faire de nouvelles relations amicales</label>
                                        <label className="yes-no-checkbox float-right">
                                            <input className="yes-no-checkbox__input" checked="" type="checkbox" name="" id="" ></input>
                                            <div className="yes-no-checkbox__checkbox"></div>
                                        </label>
                                    </div>
                                </section>
                                <section className="control">
                                    <Link to="#" className="float-left prev"></Link>
                                    <Link to="#" className="float-right next"></Link>
                                </section>
                        </fieldset>
                        <fieldset className="stepfour">
                            <section className="title">
                                <h2 className="ttr">Mes Activités</h2>
                            </section>
                            <section className="FormSignUp">
                                <div className="form-group">
                                    <label className="sttr">Mes centres d’intérêt :</label>
                                    <div className="input-group">
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Cinéma</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Bien-être</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Art</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Lecture</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Théâtre</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Concert</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Mode</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Oénologie</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Beauté</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Gastronomie</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Photographie</button>
                                        </span>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="sttr">Mes centres d’intérêt :</label>
                                    <div className="input-group">
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Football</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Natation</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Golf</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Yoga</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Fitness</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Equitation</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Rugby</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Tennis</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Voile</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Fumming</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Danse</button>
                                        </span>
                                    </div>
                                </div>
                            </section>
                            <div className="white"></div>
                            <section className="control">
                                <Link to="#" className="float-left prev"></Link>
                                <Link to="#" className="float-right next"></Link>
                            </section>
                        </fieldset>
                        <fieldset className="stepfive">
                            <section className="title">
                                <h2 className="ttr">En savoir plus</h2>
                            </section>
                            <section className="FormSignUp">
                                <div className="form-group">
                                    <label className="sttr">Je suis :</label>
                                    <div className="input-group">
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Un homme</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Une femme</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="sttr">Je souhaite rencontrer</label>
                                    <div className="input-group">
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Un homme</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Une femme</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Sans préférence</button>
                                        </span>
                                    </div>
                                </div>
                            </section>
                            <div className="white"></div>
                            <section className="control">
                                <Link to="#" className="float-left prev"></Link>
                                <Link to="#" className="float-right next"></Link>
                            </section>
                        </fieldset>
                        <fieldset className="stepsix">
                            <section className="title">
                                <h2 className="ttr">Quoi d'autre ?</h2>
                            </section>
                            <section className="FormSignUp">
                                <div className="form-group">
                                    <label className="sttr">Mes Vacances:</label>
                                    <div className="input-group">
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Bien-être</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Mer</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Océan</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Campagne</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Montagne</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Aventure</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Festif</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Sportif</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <button type="button" className="btn" data-color="primary">Luxe</button>
                                            <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Croisière</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Culture</button>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">Campagne</button>
                                        </span>
                                    </div>
                                </div>
                            </section>
                            <div className="white"></div>
                            <section className="control">
                                <Link to="#" className="float-left prev"></Link>
                                <Link to="#" className="float-right next"></Link>
                            </section>
                        </fieldset>
                        <fieldset className="stepseven">
                            <section className="title">
                                <h2 className="ttr">Des causes?</h2>
                            </section>
                            <section className="FormSignUp">
                                <div className="form-group sept">
                                    <label className="sttr">Choisissez la cause qui vous est chère</label>
                                    <p className="para">Vous contribuez à faire un don dans la vraie vie. Merci pour elle(s) </p>
                                    <div className="input-group">
                                        <span className="button-checkbox input-group-btn">
                                        <button type="button" className="btn" data-color="primary">
                                            <img src="./assets/img/profil/4.png" alt=""></img>
                                        </button>
                                        <p className="cause">Sauvegarde <br/>des océans</p>
                                        <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                        <button type="button" className="btn" data-color="primary">
                                            <img src="./assets/img/profil/3.png" alt=""></img>
                                        </button>
                                        <p className="cause">Reforestation</p>
                                        <input type="checkbox" className="hidden" />
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                        <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">
                                                <img src="./assets/img/profil/1.png" alt=""></img>
                                            </button>
                                            <p className="cause">Agroforesterie</p>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">
                                                <img src="./asets/img/profil/2.png" alt=""></img>
                                            </button>
                                            <p className="cause">Naturellement<br/>Bio</p>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">
                                                <img src="./assets/img/profil/3.png" alt=""></img>
                                            </button>
                                            <p className="cause">Theme asoss</p>
                                        </span>
                                        <span className="button-checkbox input-group-btn">
                                            <input type="checkbox" className="hidden"/>
                                            <button type="button" className="btn" data-color="primary">
                                                <img src="./assets/img/profil/5.png" alt=""></img>
                                            </button>
                                            <p className="cause">Rush / Abeille</p>
                                        </span>
                                    </div>
                                </div>
                            </section>
                            <div className="white"></div>
                            <section className="control">
                                <Link to="#" className="float-left prev"></Link>
                                <Link to="#" className="float-right next"></Link>
                            </section>
                        </fieldset>
                        <fieldset className="stepeight">
                            <section className="title">
                                <h2 className="ttr">Cheessse!!</h2>
                            </section>
                            <section className="FormSignUp">
                                <div className="form-group">
                                    <label className="sttr">Une photo de profil c'est mieux pour être un ou une parfait(e) Cheri(e)</label>   
                                </div>
                                <div className="pictures">
                                    <div className="Gpic">
                                        <img src="./assets/img/icones-ajouter-photo.svg"/>
                                    </div>
                                    <div className="Ppic">
                                        <div className="Ppic1">
                                            <img src="./assets/img/icones-ajouter-photo.svg"/>
                                        </div>
                                        <div className="Ppic2">
                                            <img src="./assets/img/icones-ajouter-photo.svg"/>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className="info d-flex">
                                <div className="pic">
                                    <img src="./assets/img/photo.svg"/>
                                </div>
                                <p className="para">
                                    Positionne toi bien devant l’objectif et pas trop près bien sûr. Choisis un fond uni c'est idéal et n'oublies pas de t'orienter vers une belle lumière :) Voila le tour est joué!
                                </p>
                            </section>
                            <div className="white"></div>
                            <section className="control">
                                <Link to="#" className="float-left prev"></Link>
                                <Link to="#" className="float-right next"></Link>
                            </section>
                        </fieldset>
                        <fieldset className="stepnine">
                            <div className="title">
                                <h2 className="ttr">A propos de moi</h2>
                            </div>
                            <div className="FormSignUp">
                                <div className="form-group">
                                    <label className="sttr">Me décrire en quelques mots (pas de crainte, personne n'est là pour te juger)</label>
                                    <textarea className="form-control" id="moi" placeholder="Raconte moi un maximum de choses sur toi ! "></textarea>
                                </div>
                                <div className="erreur">
                                    <label className="box">JE VALIDE LES CGU ET LA POLITIQUE CONFIDENTIALITE
                                        COLLECTE ET TRAITEMENT DES DONNEES
                                        <input type="checkbox" checked="checked"></input>
                                        <span className="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                            <div className="boutonSub">
                                <button type="submit" className="btn btn-default">Je valide</button>
                            </div>
                        </fieldset>
                    </form>
                </main>
            </div>
        )
    }
}
export default Signupparain;